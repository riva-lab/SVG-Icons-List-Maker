unit fm_main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, ExtCtrls,
  StdCtrls, ActnList, Buttons, Spin, FileUtil, LazFileUtils, LazUTF8,
  BGRASVGImageList, BCSVGViewer, Types, Math,
  SVGList;

type

  { TfmMain }

  TfmMain = class(TForm)
    ImageListA:     TImageList;
    tbSVGList:      TToolBar;
    tmrUpdate:      TTimer;
    cbSize:         TComboBox;
    btnUpdate:      TButton;
    cbAutoupdate:   TCheckBox;
    Panel1:         TPanel;
    lbInfo:         TLabel;
    pViewer:        TPanel;
    gbViewer:       TGroupBox;
    trBackground:   TTrackBar;
    Panel3:         TPanel;
    lvSvg:          TListView;
    Panel4:         TPanel;
    ActionList1:    TActionList;
    acAddMode:      TAction;
    acDelete:       TAction;
    acSaveList:     TAction;
    Button2:        TButton;
    Button3:        TButton;
    SpeedButton1:   TSpeedButton;
    acMoveUp:       TAction;
    acMoveDown:     TAction;
    Button1:        TButton;
    Button4:        TButton;
    Button5:        TButton;
    acClear:        TAction;
    Button6:        TButton;
    acOpenList:     TAction;
    dlgSVGListOpen: TOpenDialog;
    dlgSVGListSave: TSaveDialog;
    gbList:         TGroupBox;
    Panel5:         TPanel;
    acUpdate:       TAction;
    Panel6:         TPanel;
    gbToolbar:      TGroupBox;
    Panel7:         TPanel;
    lbDir:          TLabel;
    lbFiles:        TLabel;
    acAddAll:       TAction;
    Button7:        TButton;
    lbTime:         TLabel;
    acUnpackList:   TAction;
    acExportPNG:    TAction;
    Button8:        TButton;
    btnUpdate1:     TButton;
    pbExportPNG:    TProgressBar;
    cbPNGSize:      TComboBox;
    Bevel1:         TBevel;
    Bevel2:         TBevel;
    tmrVisibility:  TTimer;
    ImageListD:     TImageList;
    cbToolButton:   TCheckBox;
    seDisableLevel: TSpinEdit;
    lbDisabled:     TLabel;

    procedure FormCreate(Sender: TObject);
    procedure cbAutoupdateChange(Sender: TObject);
    procedure tbClick(Sender: TObject);
    procedure trBackgroundChange(Sender: TObject);
    procedure cbSizeChange(Sender: TObject);
    procedure actionExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure trBackgroundMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure acUpdateExecute(Sender: TObject);
    procedure tbSVGListMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure FormDropFiles(Sender: TObject; const FileNames: array of String);
    procedure lvSvgDblClick(Sender: TObject);
    procedure tmrVisibilityTimer(Sender: TObject);
    procedure lvSvgItemChecked(Sender: TObject; Item: TListItem);
    procedure seDisableLevelChange(Sender: TObject);

    procedure selectorUpdate(Sender: TObject = nil);

  private
    procedure infoBar(AStr: String; ASuccess: Boolean = True);
    procedure lbSVGSizeClick(Sender: TObject);
    procedure listSVGImport(AFilename: String);
    procedure listSVGIndicesUpdate;
    procedure StartMeasureRenderTime;
    procedure StopMeasureRenderTime;
    procedure initPanelSVGViewer;

  public

  end;

var
  fmMain:    TfmMain;
  tb:        array of TToolButton;
  bcSvgView: array of TBCSVGViewer;
  lbSvgSize: array of TLabel;
  mySvgList: TSVGList;
  workDir:   String;
  renderT:   TDateTime;
  lvChecked: Boolean = False;

implementation

{$R *.lfm}

function cb(ACondition: Boolean; T, F: Variant): Variant;
  begin
    if ACondition then Result := T else Result := F;
  end;


{ TfmMain }

procedure TfmMain.initPanelSVGViewer;
  var
    i: Integer;
  begin
    SetLength(bcSvgView, cbSize.Items.Count);
    SetLength(lbSvgSize, cbSize.Items.Count);

    for i := 0 to cbSize.Items.Count - 1 do
      begin
      // create svg viewer box
      bcSvgView[i] := TBCSVGViewer.Create(fmMain);

      bcSvgView[i].Parent       := pViewer;
      bcSvgView[i].Cursor       := crHandPoint;
      bcSvgView[i].ColorOpacity := 0;
      bcSvgView[i].OnClick      := @lbSVGSizeClick;
      bcSvgView[i].Tag          := i;

      bcSvgView[i].Constraints.MinHeight := StrToInt(cbSize.Items[i]);
      bcSvgView[i].Constraints.MaxHeight := StrToInt(cbSize.Items[i]);
      bcSvgView[i].Constraints.MinWidth  := StrToInt(cbSize.Items[i]);
      bcSvgView[i].Constraints.MaxWidth  := StrToInt(cbSize.Items[i]);

      bcSvgView[i].BorderSpacing.CellAlignHorizontal := ccaCenter;
      bcSvgView[i].BorderSpacing.CellAlignVertical   := ccaCenter;

      // create label to show resolution of rendered svg
      lbSvgSize[i] := TLabel.Create(fmMain);

      lbSvgSize[i].Parent    := pViewer;
      lbSvgSize[i].Caption   := cbSize.Items[i];
      lbSvgSize[i].Color     := clForm;
      lbSvgSize[i].Cursor    := crHandPoint;
      lbSvgSize[i].Alignment := taCenter;
      lbSvgSize[i].OnClick   := @lbSVGSizeClick;
      lbSvgSize[i].Tag       := i;
      end;

    // init toolbar with first smallest size
    lbSVGSizeClick(lbSvgSize[0]);
  end;

procedure TfmMain.selectorUpdate(Sender: TObject);
  var
    i: Integer;
  begin
    if Length(tb) < mySvgList.Count then
      SetLength(tb, mySvgList.Count);

    for i := 0 to mySvgList.Count - 1 do
      begin
      if tb[i] = nil then
        begin
        tb[i]         := TToolButton.Create(fmMain);
        tb[i].Parent  := tbSVGList;
        tb[i].OnClick := @tbClick;
        end;

      tb[i].Hint       := mySvgList.SVGName[i];        // use Hint to store SVG-filename
      tb[i].Tag        := mySvgList.Strings[i].Length; // use Tag to store size of SVG-file
      tb[i].Enabled    := cbToolButton.Checked;
      tb[i].ImageIndex := i;
      end;

    // remove buttons for unavailable files
    while mySvgList.Count < tbSVGList.ButtonCount do
      FreeAndNil(tb[tbSVGList.ButtonCount - 1]);
  end;

procedure TfmMain.listSVGImport(AFilename: String);
  var
    i: Integer;
  begin
      try
      mySvgList.LoadFromFile(AFileName);
      lvSvg.Clear;

      // add data to ListView
      for i := 0 to mySvgList.Count - 1 do
        begin
        lvSvg.AddItem('', nil);
        lvSvg.Items[i].SubItems.Add(IntToStr(i));
        lvSvg.Items[i].SubItems.Add(mySvgList.SVGName[i]);
        lvSvg.Items[i].SubItems.Add(mySvgList.Strings[i]);
        lvSvg.Items[i].SubItemImages[0] := i;

        // auto width
        lvSvg.AutoWidthLastColumn := False;
        lvSvg.AutoWidthLastColumn := True;
        end;

      finally
      selectorUpdate;
      end;
  end;

procedure TfmMain.listSVGIndicesUpdate;
  var
    i: Integer;
  begin
    if lvSvg.Items.Count > 0 then
      for i := 0 to lvSvg.Items.Count - 1 do
        lvSvg.Items[i].SubItems[0] := IntToStr(i);
  end;


procedure TfmMain.StartMeasureRenderTime;
  begin
    renderT := Now;
  end;

procedure TfmMain.StopMeasureRenderTime;
  begin
    lbTime.Caption := Format('Rendered in %s%d ms',
      [LineEnding, DateTimeToTimeStamp(Now - renderT).Time]);
  end;

procedure TfmMain.infoBar(AStr: String; ASuccess: Boolean);
  begin
    lbInfo.Color   := cb(ASuccess, $90F090, $9090F0);
    lbInfo.Caption := AStr;
  end;


procedure TfmMain.FormCreate(Sender: TObject);
  var
    i: Integer;
  begin
    mySvgList             := TSVGList.Create(ImageListA, ImageListD);
    trBackground.Position := 240;

    seDisableLevelChange(Sender);
    cbPNGSize.Items.Assign(cbSize.Items);
    initPanelSVGViewer;
  end;

procedure TfmMain.FormShow(Sender: TObject);
  begin
    lvSvg.Column[0].Width     := cbAutoupdate.Height;
    lvSvg.Checkboxes          := True;
    lvSvg.AutoWidthLastColumn := True;
    lbFiles.Caption           := '';

    pbExportPNG.Constraints.MaxHeight := cbPNGSize.Height;
    pbExportPNG.Constraints.MinHeight := cbPNGSize.Height;

    FormDropFiles(Sender, [cb(ParamCount = 0, ParamStr(0), ParamStr(1))]);
  end;

procedure TfmMain.FormDropFiles(Sender: TObject;
  const FileNames: array of String);
  begin
    case UTF8LowerCase(ExtractFileExt(FileNames[0])) of

      '.svglist':
        begin
        listSVGImport(FileNames[0]);
        actionExecute(nil);
        workDir                 := mySvgList.Path;
        lbDir.Caption           := mySvgList.Path;
        dlgSVGListOpen.FileName := FileNames[0];
        Exit;
        end;

      '.svg', '.exe':
        workDir := ExtractFilePath(FileNames[0]);

      else
        if DirectoryExistsUTF8(FileNames[0]) then
          workDir := FileNames[0] + DirectorySeparator
        else
          Exit;
      end;

    mySvgList.Path := workDir;
    acUpdate.Execute;
  end;

procedure TfmMain.tmrVisibilityTimer(Sender: TObject);
  var
    n: Integer;
  begin
    n := lvSvg.ItemIndex;

    lbDisabled.Enabled     := not cbToolButton.Checked;
    seDisableLevel.Enabled := not cbToolButton.Checked;
    acAddAll.Enabled       := mySvgList.Count > 0;
    acExportPNG.Enabled    := acAddAll.Enabled and cbPNGSize.Visible;
    acMoveUp.Enabled       := n > 0;
    acMoveDown.Enabled     := (n >= 0) and (n < lvSvg.Items.Count - 1);
    acDelete.Enabled       := (n >= 0) or lvChecked;
    acClear.Enabled        := lvSvg.Items.Count > 0;
    acSaveList.Enabled     := acClear.Enabled;
    acUnpackList.Enabled   := acClear.Enabled and FileExistsUTF8(dlgSVGListOpen.FileName);
  end;


procedure TfmMain.actionExecute(Sender: TObject);
  var
    i, n:        Integer;
    delSelected: Boolean;
    dir:         String;

  function IsInList(AIndex: Integer): Boolean;
    var
      a: Integer;
    begin
      if lvSvg.Items.Count > 0 then
        for a := 0 to lvSvg.Items.Count - 1 do
          if lvSvg.Items[a].SubItemImages[0] = AIndex then
            Exit(True);
      Result := False;
    end;

  begin
    BeginFormUpdate;
    n := lvSvg.ItemIndex;

    if Sender <> nil then
      case TComponent(Sender).Name of

        'acAddAll':
          //if mySvgList.Count > 0 then
          begin

          for i := 0 to mySvgList.Count - 1 do
            if not IsInList(i) then           // add item if not in list
              begin
              n := lvSvg.Items.Count;         // add new item in the very end
              lvSvg.AddItem('', nil);
              lvSvg.Items[n].SubItems.Add(IntToStr(n));
              lvSvg.Items[n].SubItems.Add(mySvgList.SVGName[i]);
              lvSvg.Items[n].SubItems.Add(mySvgList.Strings[i]);
              lvSvg.Items[n].SubItemImages[0] := i;
              end;

          lvSvg.AutoWidthLastColumn := False;
          lvSvg.AutoWidthLastColumn := True;
          end;

        'acMoveUp':
          //if n > 0 then
          begin
          lvSvg.Items.Move(n, n - 1);

          // force update images, it can be a bug of TListView (?)
          lvSvg.Items[n].SubItemImages[0]     := lvSvg.Items[n].SubItemImages[0];
          lvSvg.Items[n - 1].SubItemImages[0] := lvSvg.Items[n - 1].SubItemImages[0];
          end;

        'acMoveDown':
          //if (n >= 0) and (n < lvSvg.Items.Count - 1) then
          begin
          lvSvg.Items.Move(n, n + 1);

          // force update images, it can be a bug of TListView (?)
          lvSvg.Items[n].SubItemImages[0]     := lvSvg.Items[n].SubItemImages[0];
          lvSvg.Items[n + 1].SubItemImages[0] := lvSvg.Items[n + 1].SubItemImages[0];
          end;

        'acDelete':
          begin
          delSelected := True;
          i           := 0;
          while i < lvSvg.Items.Count do
            if lvSvg.Items[i].Checked then
              begin
              lvSvg.Items[i].Delete;
              delSelected := False;
              end
            else
              Inc(i);

          if delSelected and (n >= 0) then
            begin
            lvSvg.Selected.Delete;
            lvSvg.ItemIndex := cb(n > lvSvg.Items.Count - 1, n - 1, n);
            end;

          lvSvgItemChecked(nil, nil);
          end;

        'acClear':
          begin
          lvSvg.Clear;
          lvSvgItemChecked(nil, nil);
          dlgSVGListOpen.FileName := '';
          end;

        'acOpenList':
          begin
          dlgSVGListOpen.InitialDir := workDir;
          if dlgSVGListOpen.Execute then
            begin
            listSVGImport(dlgSVGListOpen.FileName);
            infoBar(Format('The list <%s> was successfully loaded',
              [ExtractFileNameOnly(dlgSVGListOpen.FileName)]));
            end;
          end;

        'acSaveList':
          //if lvSvg.Items.Count > 0 then
          with TSVGList.Create do
              try
              for i := 0 to lvSvg.Items.Count - 1 do
                Add(lvSvg.Items[i].SubItems[1], lvSvg.Items[i].SubItems[2]);

              dlgSVGListSave.InitialDir := workDir;
              if dlgSVGListSave.Execute then
                begin
                SaveToFile(dlgSVGListSave.FileName);
                infoBar(Format('The list <%s> was successfully saved',
                  [ExtractFileNameOnly(dlgSVGListSave.FileName)]));
                end;

              finally
              Free;
              end;

        'acUnpackList':
          //if (lvSvg.Items.Count > 0) and (FileExistsUTF8(dlgSVGListOpen.FileName)) then
          begin
          for i := 0 to lvSvg.Items.Count - 1 do
            with TStringList.Create do
                try
                Add(lvSvg.Items[i].SubItems[2]);
                dir := ExtractFilePath(dlgSVGListOpen.FileName) +
                  ExtractFileNameOnly(dlgSVGListOpen.FileName) + '-svglist' +
                  DirectorySeparator;
                if not DirectoryExistsUTF8(dir) then CreateDirUTF8(dir);
                SaveToFile(dir + lvSvg.Items[i].SubItems[1] + '.svg');
                finally
                Free;
                end;

          dir := DirectorySeparator + ExtractFileNameOnly(dlgSVGListOpen.FileName) + '-svglist';
          infoBar(Format('Successfully unpacked to sub-directory <%s>', [dir]));
          end;

        'acExportPNG':
          //if mySvgList.Count > 0 then
          if not InRange(StrToIntDef(cbPNGSize.Text, 0), 16, 2048) then
            ShowMessage('Specify size value as correct integer number in range 16 ... 2048')
          else
            begin
            cbPNGSize.Visible   := False;
            pbExportPNG.Visible := True;

            n := StrToIntDef(cbPNGSize.Text, 16);
            EndFormUpdate;

            // export dir 'png'
            dir := workDir + 'png' + DirectorySeparator;
            if not DirectoryExistsUTF8(dir) then CreateDirUTF8(dir);

            // subdir <size>
            dir += cbPNGSize.Text + DirectorySeparator;
            if not DirectoryExistsUTF8(dir) then CreateDirUTF8(dir);

            StartMeasureRenderTime;

            for i := 0 to mySvgList.Count - 1 do
              with mySvgList.GetBGRABitmap(i, n) do
                  try
                  SaveToFile(dir + mySvgList.SVGName[i] + '.png');
                  finally
                  Free;
                  pbExportPNG.Max      := mySvgList.Count - 1;
                  pbExportPNG.Position := i;
                  Application.ProcessMessages;
                  end;

            BeginFormUpdate;
            cbPNGSize.Visible   := True;
            pbExportPNG.Visible := False;
            StopMeasureRenderTime;

            dir := DirectorySeparator + 'png' + DirectorySeparator + cbPNGSize.Text;
            infoBar(Format('Successfully exported to sub-directory <%s>', [dir]));
            end;
        end;

    listSVGIndicesUpdate; // indices must be equal to item position in list

    gbList.Caption := Format('Icons list: %d item%s',
      [lvSvg.Items.Count, cb(lvSvg.Items.Count = 1, '', 's')]);

    EndFormUpdate;
  end;

procedure TfmMain.acUpdateExecute(Sender: TObject);
  var
    files: TStringList;
    i:     Integer;
  begin
    BeginFormUpdate;
    StartMeasureRenderTime;

    files := FindAllFiles(workDir, '*.svg', False);
    if files = nil then Exit;
    files.Sort;

    lbDir.Caption   := mySvgList.Path;
    lbFiles.Caption := Format('%d file%s', [files.Count, cb(files.Count = 1, '', 's')]);

    // prevent rendering after each single changing
    mySvgList.Rendering := False;

    if files.Count > 0 then
      for i := 0 to files.Count - 1 do
        mySvgList.Add(ExtractFileNameOnly(files[i]), GetFileAsString(files[i]));

    // delete items which is unavailable now
    mySvgList.DeleteUnavailableFiles;

    selectorUpdate;

    // re-enable rendering
    mySvgList.Rendering := True;

    EndFormUpdate;
    StopMeasureRenderTime;
  end;


procedure TfmMain.cbAutoupdateChange(Sender: TObject);
  begin
    tmrUpdate.Enabled := cbAutoupdate.Checked;
  end;

procedure TfmMain.cbSizeChange(Sender: TObject);
  var
    size:   Integer;
    _label: TLabel;
  begin
      try
      BeginFormUpdate;
      StartMeasureRenderTime;
      mySvgList.Rendering := False;
      size                := StrToInt(cbSize.Text);

      finally
      mySvgList.RenderSize      := size;
      tbSVGList.ButtonHeight    := size + 12;
      tbSVGList.ButtonWidth     := size + 12;
      lvSvg.Column[1].Width     := size + 5 * Canvas.GetTextWidth('0');
      lvSvg.AutoWidthLastColumn := False;
      lvSvg.AutoWidthLastColumn := True;

      for _label in lbSvgSize do
        _label.Color := cb(cbSize.Text = _label.Caption, $90F090, clForm);

      mySvgList.Rendering := True;
      EndFormUpdate;
      StopMeasureRenderTime;
      end;
  end;

procedure TfmMain.seDisableLevelChange(Sender: TObject);
  begin
    mySvgList.DisabledLevel := seDisableLevel.Value;
    mySvgList.ForceRendering;
    selectorUpdate;
  end;


procedure TfmMain.lvSvgDblClick(Sender: TObject);
  var
    a: TBCSVGViewer;
  begin
    if lvSvg.ItemIndex < 0 then Exit;

    for a in bcSvgView do
      a.SVGString := lvSvg.Selected.SubItems[2];

    gbViewer.Caption := Format('Preview of icon:   %s   %d bytes',
      [lvSvg.Selected.SubItems[1], lvSvg.Selected.SubItems[2].Length]);
  end;

procedure TfmMain.lvSvgItemChecked(Sender: TObject; Item: TListItem);
  var
    _item: TListItem;
  begin
    lvChecked := False;
    for _item in lvSvg.Items do
      if _item.Checked then
        begin
        lvChecked := True;
        Exit;
        end;
  end;


procedure TfmMain.tbClick(Sender: TObject);
  var
    i, last, k: Integer;
    a:          TBCSVGViewer;
  begin
    i := TToolButton(Sender).ImageIndex;

    for a in bcSvgView do
      a.SVGString := mySvgList[i];

    gbViewer.Caption := Format('Preview of file:   %s.svg   %d bytes',
      [TToolButton(Sender).Hint, TToolButton(Sender).Tag]);

    // add item to ListView
    if acAddMode.Checked then
      begin
      last := lvSvg.Items.Count - 1;

      if last >= 0 then
        for k := 0 to last do
          if lvSvg.Items[k].SubItemImages[0] = i then Exit; // already in list

      lvSvg.AddItem('', nil);
      lvSvg.Items[last + 1].SubItems.Add(IntToStr(last + 1));
      lvSvg.Items[last + 1].SubItems.Add(mySvgList.SVGName[i]);
      lvSvg.Items[last + 1].SubItems.Add(mySvgList.Strings[i]);
      lvSvg.Items[last + 1].SubItemImages[0] := i;

      lvSvg.AutoWidthLastColumn := False;
      lvSvg.AutoWidthLastColumn := True;

      lvSvg.ItemIndex := last + 1;       // select new item
      lvSvg.Selected.MakeVisible(False); // and scroll to show it

      actionExecute(nil);
      end;
  end;

procedure TfmMain.lbSVGSizeClick(Sender: TObject);
  begin
    cbSize.ItemIndex := TComponent(Sender).Tag;
    cbSizeChange(Sender);
  end;

procedure TfmMain.tbSVGListMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
  begin
    cbSize.ItemIndex := EnsureRange(
      cbSize.ItemIndex + Abs(WheelDelta) div WheelDelta, 0, cbSize.Items.Count);
    cbSizeChange(Sender);
  end;


procedure TfmMain.trBackgroundChange(Sender: TObject);
  begin
    pViewer.Color   := trBackground.Position * $10101;
    tbSVGList.Color := pViewer.Color;
  end;

procedure TfmMain.trBackgroundMouseWheel(Sender: TObject; Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
  begin
    trBackground.Position := trBackground.Position + abs(WheelDelta) div WheelDelta;
  end;


end.
