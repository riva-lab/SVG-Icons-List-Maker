unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  SysUtils, Forms, Controls, ComCtrls, StdCtrls, SVGList;

type

  { TForm1 }

  TForm1 = class(TForm)
    ToolBar1:    TToolBar;
    ImageList1:  TImageList;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    Label1:      TLabel;
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

var
  Form1:     TForm1;
  mySvgList: TSVGList;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
  begin
    // создаем объект
    mySvgList := TSVGList.Create(ImageList1);

    // загружаем иконки из ресурса с именем EXAMPLE
    mySvgList.LoadFromResource('EXAMPLE');

    // задаем целевой размер растровой иконки
    mySvgList.RenderSize := 32;

    // запускаем однократно рендеринг векторных иконок в растровые изображения в ImageList1
    mySvgList.ForceRendering;

    // разрешить рендеринг по изменению свойств
    //mySvgList.Rendering := True;

    // автоматический расчет размера иконки в зависимости от системного PPI
    //mySvgList.RenderSize := Scale96ToScreen(16);

    Label1.Caption := Format('Render size: %d', [mySvgList.RenderSize]);
  end;

end.
