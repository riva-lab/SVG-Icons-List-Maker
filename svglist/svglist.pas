﻿{ SVGList.pas
  -----------
  A FreePascal module that implements a list of SVG icons
  instead of a regular bitmap
  -----------

  MIT License
  -----------
  Copyright (c) 2023 Riva

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to
  deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
}

unit SVGList;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, LazFileUtils, FileUtil, Controls, LCLType, Graphics,
  BGRASVGImageList, BGRABitmapTypes, BGRABitmap;

type

  { TSVGList
    --------
    A class that implements a list of SVG icons.
    Allows you to include SVG icons in your project.
    So this allows you to change the icon resolution on the fly.
    Features:
    - loading from and saving to a simple text file .svglist
    - loading from resources
    - loading from stream
    - rendering icons to a standard TImageList
    - rendering of disabled icons with user-set luminosity level
    - operations with a list of icons
    --------
    Класс для работы с SVG-иконками.
    Предназначен для внедрения в проект иконок в векторном формате SVG.
    Это дает возможность изменять размер иконок в приложении на лету.
    Возможности:
    - загрузка и сохранение в файл - компактный список .svglist
    - загрузка из ресурсов
    - загрузка из потока
    - рендеринг иконок в стандартный TImageList
    - рендеринг обесцвеченных "отключенных" иконок (яркость настраивается)
    - операции со списком иконок
    --------
    (с) Riva, 2023.09.30
    https://riva-lab.gitlab.io
  }

  TSVGList = class(TStringList)

  public
  const
    Version: String = '1.0'; // must be equal to this file version

  private
    FSVGName:          TStringList;
    FBGRASVGImageList: TBGRASVGImageList;
    FImagesActive:     TImageList;
    FImagesDisabled:   TImageList;

    FPath:          String;
    FRenderSize:    Integer;
    FRendering:     Boolean;
    FDisabledLevel: Byte;

    function GetSVGName(Index: Integer): String;
    procedure SetSVGName(Index: Integer; AValue: String);
    procedure SetRenderSize(AValue: Integer);
    procedure SetRendering(AValue: Boolean);

    procedure RenderSVGToLists;
    procedure UpdateBGRASVGImageList;

  public

    constructor Create(AImageListActive: TImageList = nil; AImageListDisabled: TImageList = nil);
    destructor Destroy; override;

    procedure LoadFromString(const AStr: String);
    procedure LoadFromFile(const AFilename: String); override;
    procedure LoadFromStream(Stream: TStream); override;
    procedure LoadFromResource(const ResName: String; ResType: PChar = RT_RCDATA);
    procedure SaveToFile(const AFilename: String); override;

    procedure Clear; override;
    procedure Delete(Index: Integer); override;
    function Add(const AName, ASVG: String): Integer;

    procedure ForceRendering;
    function GetBGRABitmap(Index: Integer; ARenderSize: Integer = 0): TBGRABitmap;

    // (!) use for editor only: remove from list items whose corresponding files are not available
    function DeleteUnavailableFiles: Boolean;


    // (!) use for editor only: work path where current files are located
    property Path: String read FPath write FPath;

    // name of SVG icon (based on filename)
    property SVGName[Index: Integer]: String read GetSVGName write SetSVGName;

    // container for SVG icons
    property BGRASVGImageList: TBGRASVGImageList read FBGRASVGImageList write FBGRASVGImageList;

    // list of active regular images
    property ImagesActive: TImageList read FImagesActive write FImagesActive;

    // list of disabled images (usually light and grayed)
    property ImagesDisabled: TImageList read FImagesDisabled write FImagesDisabled;

    // luminosity level for disabled icons: 0..255 = darker..lighter
    property DisabledLevel: Byte read FDisabledLevel write FDisabledLevel;

    // size of raster images in lists
    property RenderSize: Integer read FRenderSize write SetRenderSize;

    // enable or disable rendering
    property Rendering: Boolean read FRendering write SetRendering;

  end;


implementation


{ TSVGList }

function TSVGList.GetSVGName(Index: Integer): String;
  begin
    Result   := '';
    if Index in [0..Count - 1] then
      Result := FSVGName[Index];
  end;

procedure TSVGList.SetSVGName(Index: Integer; AValue: String);
  begin
    if Index in [0..Count - 1] then
      FSVGName[Index] := AValue;
  end;

procedure TSVGList.SetRenderSize(AValue: Integer);
  begin
    if FRenderSize = AValue then Exit;
    FRenderSize := AValue;

    if FBGRASVGImageList <> nil then
      begin
      FBGRASVGImageList.Width  := FRenderSize;
      FBGRASVGImageList.Height := FRenderSize;
      end;

    if FImagesActive <> nil then
      begin
      FImagesActive.Width  := FRenderSize;
      FImagesActive.Height := FRenderSize;
      end;

    if FImagesDisabled <> nil then
      begin
      FImagesDisabled.Width  := FRenderSize;
      FImagesDisabled.Height := FRenderSize;
      end;

    RenderSVGToLists;
  end;

procedure TSVGList.SetRendering(AValue: Boolean);
  begin
    if FRendering = AValue then Exit;
    FRendering := AValue;

    RenderSVGToLists;
  end;

procedure TSVGList.RenderSVGToLists;
  var
    i: Integer;
    c: TBGRAPixel;
  begin
    if FBGRASVGImageList = nil then Exit;
    if (FImagesActive = nil) and (FImagesDisabled = nil) then Exit;

    if FRendering and (FBGRASVGImageList.Count > 0) then
      begin
      c := ColorToBGRA(FDisabledLevel * $10101);

      if FImagesActive <> nil then FImagesActive.Clear;
      if FImagesDisabled <> nil then FImagesDisabled.Clear;

      for i := 0 to FBGRASVGImageList.Count - 1 do
        with FBGRASVGImageList.GetBGRABitmap(i, FRenderSize, FRenderSize) do
          begin
          // add regular icons 
          if FImagesActive <> nil then
            FImagesActive.Add(Bitmap, nil);

          // add light disabled icons
          if FImagesDisabled <> nil then
            begin
            Blend(c, boLighten, True);
            FImagesDisabled.Add(Bitmap, nil);
            end;

          Free;
          end;
      end;
  end;

procedure TSVGList.UpdateBGRASVGImageList;
  var
    i: Integer;
  begin
    if FBGRASVGImageList = nil then Exit;

    for i := 0 to Count - 1 do
      if i < FBGRASVGImageList.Count then
        FBGRASVGImageList.Replace(i, Self.Strings[i])
      else
        FBGRASVGImageList.Add(Self.Strings[i]);

    // remove unused svg-s
    while Count < FBGRASVGImageList.Count do
      FBGRASVGImageList.Remove(FBGRASVGImageList.Count - 1);

    RenderSVGToLists;
  end;

constructor TSVGList.Create(AImageListActive: TImageList;
  AImageListDisabled: TImageList);
  begin
    inherited Create;
    FSVGName          := TStringList.Create;
    FBGRASVGImageList := TBGRASVGImageList.Create(nil);
    FBGRASVGImageList.TargetRasterImageList := nil;

    FImagesActive   := AImageListActive;
    FImagesDisabled := AImageListDisabled;
    FDisabledLevel  := 128;
    FRenderSize     := 16;
    FRendering      := False;
    FPath           := '';
  end;

destructor TSVGList.Destroy;
  begin
    FSVGName.Free;
    inherited Destroy;
  end;

procedure TSVGList.LoadFromString(const AStr: String);
  var
    i: Integer;
  begin
    if AStr = '' then Exit;
    Clear;

    Self.Text := AStr;

    if Count > 0 then
      begin

      // read names to other list
      for i := 0 to Count - 1 do
        if Strings[i][1] = '<' then
          FSVGName.Add(Self[i - 1]);

      // remove names from loaded list
      for i := (Count div 2) - 1 downto 0 do
        inherited Delete(i * 2);

      UpdateBGRASVGImageList;
      end;
  end;

procedure TSVGList.LoadFromFile(const AFilename: String);
  begin
    if not FileExistsUTF8(AFilename) then Exit;
    FPath := ExtractFilePath(AFilename);
    LoadFromString(ReadFileToString(AFilename));
  end;

procedure TSVGList.LoadFromStream(Stream: TStream);
  var
    strStream: TStringStream;
  begin
      try
      strStream := TStringStream.Create;
      strStream.LoadFromStream(Stream);
      LoadFromString(strStream.DataString);
      finally
      strStream.Free;
      end;
  end;

procedure TSVGList.LoadFromResource(const ResName: String; ResType: PChar);
  var
    resStream: TResourceStream;
  begin
      try
      resStream := TResourceStream.Create(HInstance, ResName, ResType);
      LoadFromStream(resStream);
      finally
      resStream.Free;
      end;
  end;

procedure TSVGList.SaveToFile(const AFilename: String);
  var
    i:   Integer;
    svg: String;
  begin
    if AFilename = '' then Exit;
    FPath := ExtractFilePath(AFilename);

    with TStringList.Create do
        try
        for i := 0 to Self.Count - 1 do
          begin
          // some optimizations
          svg := Self[i].Replace(#13, ' ').Replace(#10, ' ');
          svg := svg.Replace('> ', '>');
          svg := svg.Replace('  ', ' ').Replace('  ', ' ').Replace('  ', ' ').Replace('  ', ' ');

          Add(FSVGName[i]); // add name
          Add(svg);         // add svg
          end;

        SaveToFile(AFilename);
        finally
        Free;
        end;
  end;

procedure TSVGList.Clear;
  begin
    inherited Clear;
    FSVGName.Clear;

    UpdateBGRASVGImageList;
  end;

procedure TSVGList.Delete(Index: Integer);
  begin
    if Index in [0..Count - 1] then
      begin
      inherited Delete(Index);
      FSVGName.Delete(Index);
      UpdateBGRASVGImageList;
      end;
  end;

function TSVGList.Add(const AName, ASVG: String): Integer;
  begin
    Result := -1;
    if FSVGName.IndexOf(AName) < 0 then
      begin
      Result := FSVGName.Add(AName);
      Self.Add(ASVG);
      UpdateBGRASVGImageList;
      end;
  end;

procedure TSVGList.ForceRendering;
  var
    tmp: Boolean;
  begin
    tmp        := FRendering;
    FRendering := True;
    RenderSVGToLists;
    FRendering := tmp;
  end;

function TSVGList.GetBGRABitmap(Index: Integer; ARenderSize: Integer): TBGRABitmap;
  begin
    if ARenderSize = 0 then ARenderSize := FRenderSize;
    if ARenderSize = 0 then Exit(nil);
    Result := FBGRASVGImageList.GetBGRABitmap(Index, ARenderSize, ARenderSize);
  end;

function TSVGList.DeleteUnavailableFiles: Boolean;
  var
    i: Integer;
  begin
    Result := False;
    if FPath = '' then Exit;

    for i := Count - 1 downto 0 do
      if not FileExistsUTF8(FPath + FSVGName[i] + '.svg') then
        begin
        Delete(i);
        Result := True;
        end;
  end;

end.
